# If you want to use this, please use https://github.com/jorgebucaran/fisher instead. This is only kept for historical purposes.

# rodder
Yet Another Fish package manager

# What is rodder?
Rodder is a package manager meant for users of the fish shell to install simple commands and functions.

# How to install it?
